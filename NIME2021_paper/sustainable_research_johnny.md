Here are some notes about how we could present a framework or matrix of addressing environmental sustainability in NIME. 

Dimensions of sustainable NIME research: 

- research activity: 
    - scientific research
    - artistic practice
    - dissemination
- tools and methods: 
    - hardware
    - software
    - digital services/platforms (might also be software)
- who is involved:
    - individual/personal
    - participants
    - other artists/collaborators
    - NIME colleagues & broader academic community
    - audience/fans
- sustainable practices
    - practicing energy conservation
    - utilizing renewable energy sources
    - reducing material consumption
    - reducing waste
    - recycling
    - raising environmental awareness (via creative works)
    - not traveling
    - purchasing carbon offsets
- location/proximity to researcher
    - local/individual (home or similar)
    - local (physical) community
    - regional
    - global
    - online/virtual

...undoubtedly there are plenty of other dimensions as well, and towards a simple model not all dimensions are compelling. But perhaps this is a start towards characterizing "what" environmentally responsible NIME practice might be comprised of. 

-----

Some ways of visualizing dimensions... Another way would be a "sustainability spider plot" (2-dimensional plot for representing each dimensional on a separate axis), though this would need some work to make something that was actually useful and coherent!  

| (research activity) | Scientific research | Artistic practice | Dissemination | 
| - | - | - | - |
| Hardware | 
| Software | 
| Digital services | 

| **(sustainable practices)** | **Scientific research** | **Artistic practice** | **Dissemination** | 
| - | - | - | - |
| practicing energy conservation |
| utilizing renewable energy sources |
| reducing material consumption |
| reducing waste |
| recycling |
| raising environmental awareness (via creative works) |
| not traveling |
| purchasing carbon offsets |

-----

Content:

## Systematic review of NIME literature

To assess the degree to which environmental issues have been raised within NIME, a review of the proceedings from the NIME conference was conducted. The full proceedings are freely available from the NIME website (https://nime.org/archive) and is comprised of 1867 papers spanning each year of the conference from 2001 to 2020. The review entailed an automated search for a selection of relevant keywords followed by manual inspection of the most relevant papers. 

### Method

The keyword search of the proceedings utilizes the MacOS terminal command `mdfind` to return files matching a given query. This technique was applied by [Jensenius2014] in his review of the term ``gesture'' in NIME, and later by [Sulilvan2018] to review issues of stability and reliability in DMI design through proceedings of the NIME, ICMC, and SMC conferences. A shell script was written to automate the search and save results as a .csv document for further processing. [footnote: https://gitlab.com/johnnyvenom/eco_nime_keywords] The script is placed in the root of a directory alongside subdirectories of NIME proceedings by year. The script is run from the command line with any number of keywords to be search for listed as arguments. The resulting .csv file provides two sets of results: first, a table showing the number of papers containing each keyword for each year, and second, the filenames of the papers containing each of the keywords. 

A list of nine keywords was selected for the search: ***environment***, ***sustainability***, ***conservation***, ***ecology***, ***pollution***, ***carbon***, ***footprint***, ***biodiversity***, and ***climate***. Table 1 shows the total number of papers containing each keyword by year of proceedings.

| year | environment | sustainability | conservation | ecology | pollution | carbon | footprint | biodiversity | climate |
|:----:|:----:|:--:|:-:|:--:|:--:|:--:|:--:|:-:|:--:|
| 2001 | 8    |    |   |    |    |    |    |   |    |
| 2002 | 30   |    |   |    | 1  | 2  |    |   |    |
| 2003 | 28   |    |   |    | 1  | 2  | 1  |   |    |
| 2004 | 35   |    |   |    | 1  |    | 1  |   |    |
| 2005 | 47   |    |   | 1  |    |    |    |   |    |
| 2006 | 55   |    |   |    | 2  | 2  |    |   |    |
| 2007 | 60   | 1  |   | 3  |    | 4  | 1  |   |    |
| 2008 | 59   | 1  |   | 1  |    | 4  | 3  |   |    |
| 2009 | 54   |    |   | 4  |    | 5  |    |   |    |
| 2010 | 69   | 1  |   | 1  | 2  | 1  | 1  |   | 2  |
| 2011 | 82   | 1  |   | 6  |    | 3  | 2  |   |    |
| 2012 | 76   | 1  |   | 4  | 1  | 2  | 6  |   | 1  |
| 2013 | 70   |    |   | 2  | 1  | 1  | 2  |   | 1  |
| 2014 | 89   |    | 1 | 8  | 1  | 3  | 4  |   | 1  |
| 2015 | 46   | 1  |   | 5  |    |    | 2  | 1 | 1  |
| 2016 | 62   | 2  | 1 | 4  |    | 2  | 1  |   | 1  |
| 2017 | 69   | 1  |   | 9  | 1  | 2  | 3  |   | 3  |
| 2018 | 62   | 1  |   | 4  |    | 3  | 3  |   |    |
| 2019 | 54   | 3  |   | 4  |    | 3  |    |   | 1  |
| 2020 | 89   | 4  | 2 | 12 |    | 6  | 4  | 1 | 3  |
| **TOTALS**| **1144** | **17** | **4** | **68** | **11** | **45** | **34** | **2** | **14** |

As discussed previously, several of the words are used in different contexts that are unrelated to environmental sustainability. Most notably the term *environment* appears in more than half of all papers. To narrow the results to a small subset of papers for identification, papers were selected that contained two or more of the keywords. Given the frequency of the term *environment*, the 108 papers containing *environment* and one other keyword were also eliminated, yielding 16 papers to inspect. The breakdown of keywords and selected papers is shown in Table 2. 

| # keywords | # papers | status | 
|:--:|:----:|-------------------------------|
| 5  | 1    | keep | 
| 4  | 4    | keep |
| 3  | 10   | keep |     
| 2  | 108  | discard (contains "environment") |
| 2* | 1    | keep (doesn't contain "environment") |
| 1  | 1022 | discard |
| 1* | 50   | discard | 
