Conference calls:

Choice of service/tool:
There's a good article here: https://www.ethicalconsumer.org/technology/shopping-guide/video-conferencing?fbclid=IwAR1cfsoJGGcgypD0mktU6q-eNeDbcNR8P8c2cazyYxK5MKhjJpn22AP65lM

I am not sure how important the Conference tool is for the C02e footprint, but what we may want to consider, is if the service indirectly supports big polluters, such as Amazon, by relying on AWS (Amazon Web sevices). 
We may also condider if we can run the platform on our own server, where we would have more control over where the energy comes from.

www.meet.coop seems to offer a dedicated server service (I am in email correspondence with them).

A quick footprint calcualation of the environmental impact of participating in a virtual conference versus physical presence (based on the Ethical Consumer Article):

Video calls use 250 MB - 1.3 GB of data per participant per hour, depending on the quality.
 
The most recent reputable peer- reviewed estimate of the energy intensity of the internet found an average of 0.06 kWh per GB for 2015. Assuming that it is still halving every 2 years, it would now be around 0.015 kWh/ GB. All data does not use the same amount of energy, but (as stated) this is a rough estimate.
 
The UK grid carbon footprint is currently 0.28 kg CO2e per kWh.
That makes about 1-6 grams of CO2e per hour you spend in video calls.

NIME calculation for 1 person: 8 hours a day for four days (8 * 4 * 6): 192 grams of CO2e
Flight from Berlin to Shanghai: 2.53 tonnes of CO2e

As mentioned in this interview in German, optical cables have an energy efficiency factor of 50 over older solutions (https://www.deutschlandfunk.de/streaming-und-datenuebertragung-fuer-videokonferenzen-und.697.de.html?dram%3Aarticle_id=483915&fbclid=IwAR3U8XX_dXAGY4g3Di8ncqSctSkiYMtkNZ4oauWV4J1uIrnJ-c-lGSFa0W) 

Also, it is important to stream over wifi and to consider not choosing the highest video quality.