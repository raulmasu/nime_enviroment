In RE: planning more environmentally friendly conferences (physical and online/hybrid), and specifically @lamberto's comments to environmental statement on the [forum](https://forum.nime.org/t/nime-environmental-statement-request-for-feedback/156): 

> “More sustainable” is a bit vague, and possibly misleading. You could include environmental targets - for example reducing net conference emissions by a specific amount by a certain date, or aiming for carbon-neutral by a certain date, or supporting a flight-free conference. Or you could mention alternative delivery models - online, hybrid - that are inherently more environment-friendly.

## Some links:

- https://www.insidehighered.com/views/2019/04/18/12-scholars-share-ideas-reducing-carbon-emissions-academic-travel-opinion
- https://hiltner.english.ucsb.edu/index.php/ncnc-guide/
- https://www.thelancet.com/journals/lanplh/article/PIIS2542-5196(20)30003-6/fulltext
- https://www.researchgate.net/publication/285573035_The_Carbon_Footprint_of_Academic_Conferences_Evidence_from_the_14_th_EAAE_Congress_in_Slovenia
- [a paper](./REPORT-Climate-Change-and-Academia-ESAC-CSDH.pdf)

## And some questions: 

- value in setting strict quantifiable objects (eg., "carbon neutral by 20xx")?
- And if so, by what metrics? 