# Environmental issues officer

Main goal: Work on how to reduce the environmental footprint of the NIME community

## Tasks:

- Support local chairs in how to run a more environmentally-friendly physical conference
- Develop strategies for alternative conference organization
- Develop guidelines for reducing the environmental footprint in NIME design. 

## Officers:

- Adam Pultz Melbye
- John Sullivan
- Raul Masu

## ECO_NIME

To provide information and resources on environmental topics for NIME researchers, we have created the **ECO_NIME wiki**, which is contained in a separate repo [here](https://gitlab.com/raulmasu/eco_nime).

## Alternative conference organization

- [The semi-virtual academic conference (a short documentary) - YouTube](https://www.youtube.com/watch?v=TPtDHidVyZE)
- [Virtual socializing at academic conferences](http://www.parncutt.org/virtualsocializing.html)
- [An analysis of ways to decarbonize conference travel after COVID-19](https://www.nature.com/articles/d41586-020-02057-2?s=09)


Examples of Virtual conferences:

- [CIM19](https://sites.google.com/view/cim19/home)
- [Neuronline](https://neuronline.sfn.org/Articles/Scientific-Research/2019/Machine-Learning-in-Neuroscience-Fundamentals-and-Possibilities)

## Time zone coordination

(for planning meetings)

See: https://www.timeanddate.com/worldclock/meetingtime.html?iso=20210322&p1=165&&p2=682&p3=37&p4=187

Change `iso=yyyymmdd` in URL to intended date. 