Genaral notes regarding the environmental impact of the internet:

According to this article (https://onlinelibrary.wiley.com/doi/epdf/10.1111/jiec.12630), the energy efficiency of the internet has been roughly doubling every two years.

The UK grid carbon footprint is currently 0.28 kg CO2e per kWh (source: https://www.ethicalconsumer.org/technology/shopping-guide/video-conferencing?fbclid=IwAR1cfsoJGGcgypD0mktU6q-eNeDbcNR8P8c2cazyYxK5MKhjJpn22AP65lM)