Here a few ideas. I think that there are at least three directions that can be explored (the three points are not in order of importance):

1) fostering environmental NIMEs in the topic of the conference
2) keeping into consideration environmental issues in the organization of the conference
3) fostering environmental awareness practices in the participants

in detail

1) maybe we can always organize a track of submissions that have an environmental objective/topic. For example, installations that aim to foster a discussion or bring light on environmental issues. For instance, this year, a poster presented a soil sonification work that was "inspired" by the problem of desertification of wood soils. Another topic could be recycling material for NIME.
I think that if we join together many of these works, we can foster a debate on how to use such sonic artworks or research to increase environmental awareness outside NIME. My idea is that if we have many projects on the same track, we can learn from each other, and maybe start coordinated actions.
If not a track, we might at least include this in the topic of the conference.

2) many strategies could apply here:
- Maintain the possibility of virtual participation actually encouraging people who would have to travel long distances to NOT participate in presence. I would suggest adding a table with the travel footprint from N main cities to the location of the conference. This could increase the awareness of the environmental impact. 
- >about this i've seen some idea inthe forum about dislocated hubs we should take a look.
    - https://forum.nime.org/t/how-can-we-create-a-succesful-hybrid-conference/39/2
    - https://forum.nime.org/t/decarbonizing-the-nime-community-towards-more-sustainable-practices/41/22
    - https://forum.nime.org/t/how-can-we-make-an-even-better-virtual-conference/38/4
    


- Another thing could be promoting a NIME week green challenge during the week of NIME. It could consist of inviting participants who attend the conference in the presence to eat the vegetarian option, walk from your hotel to the conference place, and try to eat local food. Vegetarian and pedestrian suggestions can be extended to people who attend remotely. Of course, this needs to be on a voluntary base. But in general, it could help the debate on environmental topics if the conference promotes such initiative.
- I would also like to suggest Moving the NIME website to a green hosting service and decare it. So make of it a statement. I am now doing a small research on green hosting services for an environmental project we will submit to my city municipality. For now, I found a couple of options (e.g., https://www.aiso.net/ ) ,soon I will have a comparison of different services. 

3) we can organize a list of guidelines and links to tools that can allow NIME participants to increase their environmental awareness. For instance, we can suggest web hosters that have a system of carbon footprint balancing. Maybe NIME can state something like: "if you are developing some a web-based system or survey, we highly encourage using a green hosting service." And then add a list of green hosts.
In the ethic commettee also emerged the theme of reciclyng objects for different studies.
I would actually suggest to contact these green host and see if there can be some possible form of collaboration. For instance, if NIME participants or people who intend to submit to NIME can get discounts, in exchange for the advertisement.

All the points (especially 2 and 3) are connected, and I think that the two main objectives are 1) reduce the actual carbon footprint of NIME and 2) facilitating, fostering, and contributing to the ecological debate. So to be more pragmatic, I think it would be great to have a group to discuss the proposals. In particular:
- fosterning green and environmentally oriented NIME submission: how to do that? and how to facilitate collaborations?
- doing research on green web hosting and contacting the companies to try to get collaboration
- promote green alternatives in the NIME website (eventually compile a list of guidelines, in case, coordinate with the ethical officer)
- promoting recicling matherial
- promote green alternatives and remote participation green week challanges  etc during NIME.

