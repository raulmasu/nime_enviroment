 # NIME 2021 paper

 ## General info

 - Use https://www.pubpub.org/ for collaborating

- Topic: Addressing environmental issues in NIME research and introducing the [ECO_NIME wiki](https://eco.nime.org)
- Submission deadline: 22-January 2021; final submissions: 31-Jan 2021
- Length: 5000/3000 words (full/short paper)
- Format: [Pubpub](https://www.pubpub.org/) -> PDF for submission
## Preliminary outline

- Introduction - all
- Background
    - Intersection of NIME practice/music technology and environmental issues - Adam
    - NIME as a self reflective community - Raul
    - Systematic review of NIME literature - Johnny/all
    - Keyword/collocation analysis of NIME (and possibly other conferences) proceedings (see [keyword_analysis]([/NIME2021_paper/keyword_analysis](https://gitlab.com/johnnyvenom/eco_nime_keywords))
    - manual review of NIME abstracts
    - Parallel/similar communities: HCI; intermedia/interdisciplinary artistic practices (omit this - too expansive)
- ECO_NIME wiki - all
    - Tie in to HCI literature
    - How can people contribute to and use the wiki
- Discussion/Conclusion

Deadlines:
 - Raul: Wiki as a design artifact. Deadline: December 8th.
 - Adam: Intersection: December 8th
 - Johnny: preliminary paper search
 - Meeting to discuss progress: Dec 9th, 9am/2pm/3pm.