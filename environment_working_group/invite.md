# Proposed invitation email 

Greetings - 

In the few short months since we started as NIME environmental officers, we've had many conversations with friends and colleagues about issues around climate change and environmental sustainability with regard to our collective practices. To continue these conversations and to create new ones, we have raised the idea of forming an informal working/discussion group that could meet online regularly (perhaps every one or two months). 

As a start, we'd like to invite anyone interested to an initial online meeting on or around March 29th. The meeting will have an open agenda and all are free to attend. We can discuss what sort of activities might be compelling for the group to participate in, and how often and in what capacity we might meet. One possible option we have thought of is to organize it as a "book club". Being both affiliated with NIME while also working as free-lance instrument-builders, musicians and composers, we envision this group to be open to include perspectives and practices beyond specifically NIME or music technology research, that are interested in or directly address environmental and sustainability topics. 

As we are a global community spread over many time zones, if you would like to attend, please reply with your availability for March 29th and the time zone you are in. If you are unavailable March 29th, let us know other availabilities that week and we will do our best to propose a time that works for all. IF you like, please also feel free to include a sentence or two that describes your practice and interest in these topics.

For general information about NIME and the environment, you can read the [NIME Environmental Statement](https://www.nime.org/environment) and check out the [ECO_NIME Wiki](https://eco.nime.org).

Thanks and we look forward to meeting you!  
Johnny, Adam, and Raul - NIME Environmental Officers

