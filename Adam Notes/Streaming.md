NIME environmental research:

The impact of streaming: 
Glasfaserkabel ist 50mal effizienter, energiepolitisch betrachtet, als die Übertragung über das alte UMTS-System (https://www.deutschlandfunk.de/streaming-und-datenuebertragung-fuer-videokonferenzen-und.697.de.html?dram%3Aarticle_id=483915&fbclid=IwAR3U8XX_dXAGY4g3Di8ncqSctSkiYMtkNZ4oauWV4J1uIrnJ-c-lGSFa0WA)

Make sure to have your conference call on Wifi

https://www.ethicalconsumer.org/technology/shopping-guide/video-conferencing?fbclid=IwAR1cfsoJGGcgypD0mktU6q-eNeDbcNR8P8c2cazyYxK5MKhjJpn22AP65lM

- The energy efficiency of the internet has been roughly doubling every two years	
Video calls use 250 MB - 1.3 GB of data per participant per hour, depending on the quality.
 
The most recent reputable peer- reviewed estimate of the energy intensity of the internet found an average of 0.06 kWh per GB for 2015. Assuming that it is still halving every 2 years, it would now be around 0.015 kWh/ GB. All data does not use the same amount of energy, but (as stated) this is a rough estimate.
 
The UK grid carbon footprint is currently 0.28 kg CO2e per kWh.
That makes about 1-6 grams of CO2e per hour you spend in video calls.

NIME calculation for 1 person: 8 hours a day for four days (8 * 4 * 6): 192 grams of CO2e
Flight from Berlin to Shanghai: 2.06 tonnes of CO2e

Streaming / Download of music: Kyle Devine has done research on this